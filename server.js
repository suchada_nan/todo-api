const express = require('express')
const app = express()

let todos = [
    {
name:'nan',
id: 1
},
{
    name:'suchada',
    id: 2
    }
]
//select * FROM
app.get('/todos',(req,res) => {
res.send(todos)

}) //ดึงข้อมูลจาก todo

//INSERT INTO TODO
app.post('/todos',(req,res) => {
let newTodo = {
    name: 'read a book',
    id:3
}
todos.push(newTodo)
res.status(201).send()
})

app.listen(3000, () => {
    console.log('TODO API Started at port 3000')
})